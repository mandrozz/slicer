/**
 * AmFiles - a TinyMCE 4x files upload plugin
 * Released under Creative Commons Attribution 3.0 Unported License
 * License: http://creativecommons.org/licenses/by/3.0/
 * Plugin info: hello@andrey-malygin.ru
 * Author: Andrey Malygin
 *
 * Version: 0.1b released 5/02/2014
 * change @path value for your application
 */
var amFilesDialog = {

    progress : function() {
        document.getElementById("infobar").style.display = 'none';
        document.getElementById("info").innerHTML = '';
        document.getElementById("form_container").style.display = 'none';
        document.getElementById("progress").style.display = 'block';
        window.setTimeout(function(){
            document.getElementById("info").innerHTML = 'Ошибка загрузки файла. Информация для отладки:';
            document.getElementById("progress").style.display = 'none';
            document.getElementById("target").className = 'visible';
        }, 30000);
    },

    finish : function(result) {
        document.getElementById("progress").style.display = 'none';
        document.getElementById("infobar").style.display = 'block';
        document.getElementById("infobar").innerHTML = 'Загрузка завершена';

        var w = this.win();
        tinymce = w.tinymce;

        tinymce.EditorManager.activeEditor.insertContent('<a href="' + result.path +'">'+result.filename+'</a>');

        this.close();
    },

    win : function() {
        return (!window.frameElement && window.dialogArguments) || opener || parent || top;
    },

    close : function() {
        var t = this;

        function close() {
            tinymce.EditorManager.activeEditor.windowManager.close(window);
            tinymce = tinyMCE = t.editor = t.params = t.dom = t.dom.doc = null;
        }

        if (tinymce.isOpera)
            this.win().setTimeout(close, 0);
        else
            close();
    }

};
<?php
/**
 * AmFiles - a TinyMCE 4x files upload plugin
 * Released under Creative Commons Attribution 3.0 Unported License
 * License: http://creativecommons.org/licenses/by/3.0/
 * Plugin info: hello@andrey-malygin.ru
 * Author: Andrey Malygin
 *
 * Version: 0.1b released 5/02/2014
 * change @path value for your application
 */
$path = '/uploads/content/files/';
$originalFileName = $_FILES['userfile']['name'];
$fileName = str2url($_FILES['userfile']['name']);
$filePath = $_SERVER['DOCUMENT_ROOT'] . $path . $fileName;
?>

<?php
if (move_uploaded_file($_FILES['userfile']['tmp_name'], $filePath)):
?>
    <script language="javascript" type="text/javascript">
        window.parent.window.amFilesDialog.finish({
            filename:'<?php echo $originalFileName; ?>',
            path: '<?php echo $path.$fileName; ?>'
        });
    </script>
<?php
endif;

function rus2translit($string)
{
    $converter = array(

        'а' => 'a',   'б' => 'b',   'в' => 'v',
        'г' => 'g',   'д' => 'd',   'е' => 'e',
        'ё' => 'e',   'ж' => 'zh',  'з' => 'z',
        'и' => 'i',   'й' => 'y',   'к' => 'k',
        'л' => 'l',   'м' => 'm',   'н' => 'n',
        'о' => 'o',   'п' => 'p',   'р' => 'r',
        'с' => 's',   'т' => 't',   'у' => 'u',
        'ф' => 'f',   'х' => 'h',   'ц' => 'c',
        'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sch',
        'ь' => '',  'ы' => 'y',   'ъ' => '',
        'э' => 'e',   'ю' => 'yu',  'я' => 'ya',

        'А' => 'A',   'Б' => 'B',   'В' => 'V',
        'Г' => 'G',   'Д' => 'D',   'Е' => 'E',
        'Ё' => 'E',   'Ж' => 'Zh',  'З' => 'Z',
        'И' => 'I',   'Й' => 'Y',   'К' => 'K',
        'Л' => 'L',   'М' => 'M',   'Н' => 'N',
        'О' => 'O',   'П' => 'P',   'Р' => 'R',
        'С' => 'S',   'Т' => 'T',   'У' => 'U',
        'Ф' => 'F',   'Х' => 'H',   'Ц' => 'C',
        'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Sch',
        'Ь' => '',  'Ы' => 'Y',   'Ъ' => '',
        'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',

    );
    return strtr($string, $converter);
}

function str2url($str)
{
    $str = rus2translit($str);
    $str = strtolower($str);
    $str = preg_replace('~[^-a-z0-9_\.]+~u', '-', $str);
    $str = trim($str, "-");
    return $str;
}
?>

